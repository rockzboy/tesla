import { configureStore } from '@reduxjs/toolkit';
import carReducer from "../features/car/carsSlice";

export const store = configureStore({
  reducer: {
    car: carReducer
  },
});
